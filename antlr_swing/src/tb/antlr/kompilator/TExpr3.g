tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer block =0;
  Integer numer = 0;
}
prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje> start: <name;separator=\" \n\"> ";

decl  :
        ^(VAR i1=ID) {localSymbols.newSymbol($ID.text + block.toString());} -> dek(n={$ID.text + block.toString()})
    ;
    

    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> set(p1={$i1.text},p2={$e2.st})
        | ID                       -> get(p1={$ID.text})
        | INT  {numer++;}                    -> int(i={$INT.text},j={numer.toString()})
    ;
    
blocks  :^(LBR {block++; localSymbols.enterScope();} (e+=blocks | e+=expr | d+=decl)* {block--; localSymbols.leaveScope();}) -> block(name={$e}, dekl={$d});
    