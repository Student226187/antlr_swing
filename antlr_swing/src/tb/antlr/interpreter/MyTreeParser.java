package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {
	
	private LocalSymbols localSymbols = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
    protected Integer potega(int x, int y) {
    	return (int)Math.pow(x, y);
    }
    
    protected void newVar(String s) {
    	localSymbols.newSymbol(s);
    }    

    protected Integer setVar(String s, Integer e) {    	
        localSymbols.setSymbol(s, e);
        return e;
    }
    
    protected Integer getVar(String id) {
    	if(!localSymbols.hasSymbol(id)) 
    	{ 
    		throw new RuntimeException("variable is not declared: "+id);
    	}
    	return localSymbols.getSymbol(id);    	
    }
}
